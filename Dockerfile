FROM registry.gitlab.com/farvour/steam-dedicated-server-base:latest
LABEL maintainer="Thomas Farvour <tom@farvour.com>"

ARG DEBIAN_FRONTEND=noninteractive

# Perform steamcmd installations as the server user.
USER ${PROC_USER}

# This is most likely going to be the largest layer created; all the game files for the dedicated server. It is a
# good idea to do as much as possible _beyond_ this point to avoid Docker having to re-create it.
RUN echo "Downloading and installing server with steamcmd" \
    && ${SERVER_HOME}/Steam/steamcmd.sh \
    +force_install_dir ${SERVER_INSTALL_DIR} \
    +login anonymous \
    +app_update 896660 validate \
    +quit

# Mod/add-on support.
ARG BEPINEXPACK_VERSION="5.4.2202"

RUN echo "Downloading and installing the BepInExPack for Valheim modding" \
    && wget -O denikson-BepInExPack_Valheim-${BEPINEXPACK_VERSION}.zip https://gcdn.thunderstore.io/live/repository/packages/denikson-BepInExPack_Valheim-${BEPINEXPACK_VERSION}.zip \
    && unzip denikson-BepInExPack_Valheim-${BEPINEXPACK_VERSION}.zip \
    && ls -la . \
    && cp -rv BepInExPack_Valheim/* ${SERVER_INSTALL_DIR}/

# Install custom startserver script.
COPY --chown=${PROC_USER}:${PROC_GROUP} scripts/startserver-1.sh ${SERVER_INSTALL_DIR}/

# Install and then configure custom BepInEx mods.
ENV BEPINEX_PLUGINS_SRC_DIR "${SERVER_HOME}/BepInExPluginsSrc"
ENV BEPINEX_PLUGINS_DIR "${SERVER_INSTALL_DIR}/BepInEx/plugins"
ENV BEPINEX_CONFIG_DIR "${SERVER_INSTALL_DIR}/BepInEx/config"

RUN echo "Creating BepInEx plugin mods source directory" \
    && mkdir -p ${BEPINEX_PLUGINS_SRC_DIR}

COPY --chown=${PROC_USER}:${PROC_GROUP} plugins/*.dll ${BEPINEX_PLUGINS_SRC_DIR}/
COPY --chown=${PROC_USER}:${PROC_GROUP} plugins/*.zip ${BEPINEX_PLUGINS_SRC_DIR}/

RUN echo "Installing and configuring BepInEx server-side mods" \
    && cd ${BEPINEX_PLUGINS_SRC_DIR} \
    && unzip "ASharpPen-Spawn_That-1.2.15.zip" Valheim.SpawnThat.dll YamlDotNet.dll -d ${BEPINEX_PLUGINS_DIR} \
    && unzip "ASharpPen-Drop_That-3.0.8.zip" Valheim.DropThat.dll -d ${BEPINEX_PLUGINS_DIR} \
    && unzip "Advize-PlantEverything-1.18.2.zip" Advize_PlantEverything.dll -d ${BEPINEX_PLUGINS_DIR} \
    && unzip "Azumatt-DeezServerSyncBalls-1.0.5.zip" DeezServerSyncBalls.dll -d ${BEPINEX_PLUGINS_DIR} \
    && unzip "Azumatt-HelheimHarmonizer-1.0.6.zip" HelheimHarmonizer.dll -d ${BEPINEX_PLUGINS_DIR} \
    && unzip "Korppis-Spearfishing-1.0.4.zip" Spearfishing.dll -d ${BEPINEX_PLUGINS_DIR} \
    && unzip "OdinPlus-TeleportEverything-2.7.0.zip" TeleportEverything.dll -d ${BEPINEX_PLUGINS_DIR} \
    && unzip "RustyMods-FishTrap-1.0.2.zip" FishTrap.dll -d ${BEPINEX_PLUGINS_DIR} \
    && unzip "Smoothbrain-ComfortTweaks-3.3.8.zip" ComfortTweaks.dll -d ${BEPINEX_PLUGINS_DIR} \
    && unzip "Smoothbrain-SailingSpeed-1.0.3.zip" SailingSpeed.dll -d ${BEPINEX_PLUGINS_DIR} \
    && cp KrampusModBetterFood.dll ${BEPINEX_PLUGINS_DIR}/ \
    && cp KrampusModBiggerOreQueues.dll ${BEPINEX_PLUGINS_DIR}/ \
    && cp KrampusModCarryWeightModifier.dll ${BEPINEX_PLUGINS_DIR}/ \
    && echo "Done installing mods!"

# Copies initial server-side mod configuration.
COPY --chown=${PROC_USER}:${PROC_GROUP} plugins/config/*.cfg ${BEPINEX_CONFIG_DIR}/
COPY --chown=${PROC_USER}:${PROC_GROUP} plugins/config/*.yml ${BEPINEX_CONFIG_DIR}/

# Set working directory to installation directory.
WORKDIR ${SERVER_INSTALL_DIR}

# Switch back to root user to allow entrypoint to drop privileges.
USER root

# Default game server ports.
EXPOSE 2456/tcp 2456/udp
EXPOSE 2457/tcp 2457/udp
EXPOSE 2458/tcp 2458/udp

# Install custom entrypoint script.
COPY scripts/entrypoint.sh /entrypoint.sh

# Set entrypoint and default command.
ENTRYPOINT ["/usr/bin/dumb-init", "--rewrite", "15:2", "--", "/entrypoint.sh"]
CMD ["./startserver-1.sh"]
